/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guides;

import java.util.Scanner;

/**
 *
 * @author Jesse
 */
public class Temperatures {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double[] T = new double[7];
		double sum = 0;

		// Capturar las temperaturas
		for (int i = 0; i < 7; i++) {
			System.out.println("Ingrese la temperatura del día " + (i + 1) + ": ");
			T[i] = scanner.nextDouble();
			sum += T[i];
		}

		// Calcular e imprimir la temperatura promedio
		double promedio = sum / 7;
		System.out.println("La temperatura promedio es: " + promedio);

		// Formar un vector D con las diferencias respecto al promedio
		double[] D = new double[7];
		for (int i = 0; i < 7; i++) {
			D[i] = T[i] - promedio;
		}

		// Imprimir la menor temperatura y el número de día en que ocurrió
		double minTemp = T[0];
		int dia = 1;
		for (int i = 1; i < 7; i++) {
			if (T[i] < minTemp) {
				minTemp = T[i];
				dia = i + 1;
			}
		}
		System.out.println("La menor temperatura fue " + minTemp + " en el día " + dia);
	}
}
