/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guides;

/**
 *
 * @author Jesse
 */
public class BubbleSort {

	public static void main(String[] args) {
		int[] array = {5, 3, 8, 4, 2};
		bubbleSort(array);
		for (int i : array) {
			System.out.print(i + " ");
		}
	}

	public static void bubbleSort(int[] array) {
		int n = array.length;
		for (int i = 0; i < n - 1; i++) {
			for (int j = 0; j < n - i - 1; j++) {
				if (array[j] > array[j + 1]) {
					// Intercambiar array[j] y array[j+1]
					int temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
				}
			}
		}
	}
}
