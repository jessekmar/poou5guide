/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guides;

import java.util.Scanner;

/**
 *
 * @author Jesse
 */
public class SimpleMatrix {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Ingrese el tamaño de la matriz (n): ");
		int n = scanner.nextInt();
		int[][] matriz = new int[n][n];

		// Llenar la matriz
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.println("Ingrese el elemento en la posición [" + i + "][" + j + "]: ");
				matriz[i][j] = scanner.nextInt();
			}
		}

		// Imprimir las diagonales
		System.out.println("Diagonal principal: ");
		for (int i = 0; i < n; i++) {
			System.out.print(matriz[i][i] + " ");
		}
		System.out.println("\nDiagonal secundaria: ");
		for (int i = 0; i < n; i++) {
			System.out.print(matriz[i][n - 1 - i] + " ");
		}

		// Imprimir el promedio de cada renglón
		for (int i = 0; i < n; i++) {
			int sum = 0;
			for (int j = 0; j < n; j++) {
				sum += matriz[i][j];
			}
			System.out.println("\nPromedio del renglón " + i + ": " + (double) sum / n);
		}

		// Imprimir el valor más pequeño de cada columna
		for (int j = 0; j < n; j++) {
			int min = matriz[0][j];
			for (int i = 1; i < n; i++) {
				if (matriz[i][j] < min) {
					min = matriz[i][j];
				}
			}
			System.out.println("Valor más pequeño de la columna " + j + ": " + min);
		}

		// Imprimir el valor mayor en la matriz
		int max = matriz[0][0];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (matriz[i][j] > max) {
					max = matriz[i][j];
				}
			}
		}
		System.out.println("Valor mayor en la matriz: " + max);

		// Intercambiar los valores de la fila x por los valores de la fila y
		System.out.println("Ingrese la fila x a intercambiar: ");
		int x = scanner.nextInt();
		System.out.println("Ingrese la fila y a intercambiar: ");
		int y = scanner.nextInt();
		int[] temp = matriz[x];
		matriz[x] = matriz[y];
		matriz[y] = temp;

		System.out.println("Matriz después del intercambio: ");
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
	}
}
