/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guides;

/**
 *
 * @author Jesse
 */
public class MaxDiff {

	public static void main(String[] args) {
		int[] array = {1, 2, 6, 8, 5, 11, 3};
		findMaxDifference(array);
	}

	public static void findMaxDifference(int[] array) {
		int maxDiff = 0;
		int pos1 = 0, pos2 = 0;
		for (int i = 0; i < array.length - 1; i++) {
			int diff = Math.abs(array[i] - array[i + 1]);
			if (diff > maxDiff) {
				maxDiff = diff;
				pos1 = i;
				pos2 = i + 1;
			}
		}
		System.out.println("La mayor diferencia es " + maxDiff + " entre los elementos en las posiciones " + pos1 + " y " + pos2);
	}
}
