/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Guides;

import java.util.Arrays;
import java.util.LinkedHashSet;

/**
 *
 * @author Jesse
 */
public class DuplicatedElements {

	public static void main(String[] args) {
		int[] A = {10, 20, 20, 30, 30, 30, 40, 50, 50};
		removeDuplicates(A);
	}

	public static void removeDuplicates(int[] A) {
		LinkedHashSet<Integer> set = new LinkedHashSet<>();
		for (int i = 0; i < A.length; i++) {
			set.add(A[i]);
		}
		Integer[] B = set.toArray(new Integer[0]);
		System.out.println("Vector sin duplicados: " + Arrays.toString(B));
		System.out.println("Número de elementos que permanecen: " + B.length);
	}
}
